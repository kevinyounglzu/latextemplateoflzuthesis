SOURCE=$(wildcard *.tex)
INCLUDE=$(wildcard chapter/*.tex)
TARGET=$(patsubst %.tex,%.pdf,$(SOURCE))
BBL=$(patsubst %.tex,%.bbl,$(SOURCE))
AUX=$(patsubst %.tex,%.aux,$(SOURCE))

all: $(TARGET)

$(TARGET) : $(SOURCE) $(INCLUDE)
	xelatex $<

ref: $(SOURCE) $(BBL) $(INCLUDE)
	xelatex $<
	xelatex $<

$(BBL) : $(AUX)
	bibtex $<

$(AUX) : $(SOURCE)
	xelatex $<

clean:
	rm -f *.aux chapter/*.aux *.log *.blg *.bbl *.pdf *.toc *.out
